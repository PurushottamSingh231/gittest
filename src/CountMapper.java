import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

public class CountMapper extends Mapper<LongWritable, Text,Text, IntWritable> {

    private final IntWritable one = new IntWritable(1);
    private Text word = new Text();
    @Override
    public void map(LongWritable key, Text line, Context context) throws
            IOException, InterruptedException {

            String str = line.toString();
            StringTokenizer token = new StringTokenizer(str);

            while(token.hasMoreTokens()){
                word.set(token.nextToken());
                context.write(word,one);
            }

//            String []arr = str.split(" ");
//            for(String s : arr){
//                context.write(new Text(s),one);
//            }
    }
}
